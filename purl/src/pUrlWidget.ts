interface IInputEls {
    pUrlInput: HTMLInputElement;
    pUrlButton: HTMLButtonElement;
    pUrlFeedBackEl: HTMLSpanElement;
    pUrlLabel: HTMLLabelElement;
}

setTimeout(initFriendBuyPUrl, 1000);

function initFriendBuyPUrl() {

    const siblingContainer = <HTMLElement>(document.querySelector('.sc-bdfBwQ.sc-gsTCUz.bEDtzz.dFfFa-d') as HTMLElement);
    console.log(siblingContainer)
    if (siblingContainer) {
        appendWidget(siblingContainer);
    }

    function appendWidget(el: HTMLElement) {
        // Appending the widget should happen first, before querying elements
        el.insertAdjacentHTML('afterend', createWidget());

        const pUrlInput = <HTMLInputElement>document.getElementById('purl-input-link');
        const pUrlButton = <HTMLButtonElement>document.querySelector('.purl-button');
        const pUrlFeedBackEl = <HTMLSpanElement>document.querySelector('.purl-feedback');
        const pUrlLabel = <HTMLLabelElement>document.querySelector('label[for="purl-input-link"]');
        const els: IInputEls = { pUrlInput, pUrlButton, pUrlFeedBackEl, pUrlLabel };

        pUrlButton.addEventListener('click', onShareClick.bind(null, els));

        // Generate PURL from FB API
        const response = getLink()
            .then(assignReferralValue.bind(null, els))
            .catch(console.error);
        response.then(console.log);
    }

    function getLink() {
        const url = 'https://us-central1-mario-luevanos.cloudfunctions.net/api/byte/purl';
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify(getPayload()),
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then(response => response.json())
            .then(data => data.referral_link)
    }

    function assignReferralValue({ pUrlInput }: { pUrlInput: HTMLInputElement }, referralLink: { trackable_link: string }) {
        if (referralLink && referralLink.trackable_link) {
            pUrlInput.value = referralLink.trackable_link;
        }
        return referralLink;
    }

    function getPayload(): {
        email_address: string;
        widget_id: number;
    } {
        const email_address = getEmail();
        const widget_id = 73808;
        return { widget_id, email_address };
    }

    // Capture the email on the first step
    function getEmail() {
        const email = 'mluevanos@byteme.com';
        if (isValidateEmail(email)) {
            return email;
        }
        return '';
    }

    function isValidateEmail(email: string): boolean {
        const pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!email || !pattern.test(email.trim())) {
            return false;
        } else {
            return true;
        }
    }

    function onShareClick(els: IInputEls, event: MouseEvent) {
        const { pUrlInput } = els;
        navigator.clipboard.writeText(pUrlInput.value)
            .then(onCopyClipboard.bind(null, els))
            .catch(console.error);
    }

    function onCopyClipboard(els: IInputEls) {
        const { 
            pUrlButton, 
            pUrlLabel, 
            pUrlFeedBackEl
        } = els;
        pUrlLabel.innerText = 'Copied!';
        pUrlFeedBackEl.classList.add('is-copied');
        pUrlButton.classList.add('is-copied');
        pUrlFeedBackEl.addEventListener('animationend', onCopyAnimationEnd.bind(null, els));
    }

    function onCopyAnimationEnd(els: IInputEls, event: AnimationEvent) {
        console.log(event);
        const { 
            pUrlButton, 
            pUrlLabel, 
            pUrlFeedBackEl
        } = els;
        if (event.animationName === 'copied') {
            setTimeout(() => {
                pUrlButton.classList.remove('is-copied');
                pUrlFeedBackEl.classList.remove('is-copied');
                pUrlLabel.innerText = 'Copy Link';
            }, 0);
            pUrlFeedBackEl.removeEventListener('animationend', onCopyAnimationEnd.bind(null, els));
        }
    }

    function createWidget() {
        return `
        <div class="purl-friendbuy-widget">
            <h3>Personal Referral Link</h3>
            <div class="purl-text">
                <h4>Get $200 Amazon Gift Card</h4>
                <p>Give all your friends 75% off their impression kit and $100 off their aligners. In return, <strong>we’ll give you $200</strong> every time one of them orders a plan. It’s only fair. Share away and cash in on those rewards.</p>
            </div>
            <div class="purl-share">
                <h4>Share This Link</h4>
                <button class='purl-button'>
                    <div class='purl-icon'>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="16"
                            height="18.286"
                            viewBox="0 0 16 18.286"
                        >
                            <path d="M15.5,2.355,13.645.5A1.714,1.714,0,0,0,12.433,0H6.286A1.714,1.714,0,0,0,4.571,1.714V3.429H1.714A1.714,1.714,0,0,0,0,5.143V16.571a1.714,1.714,0,0,0,1.714,1.714h8a1.714,1.714,0,0,0,1.714-1.714V14.857h2.857A1.714,1.714,0,0,0,16,13.143V3.567a1.714,1.714,0,0,0-.5-1.212Zm-6,14.216H1.929a.214.214,0,0,1-.214-.214v-11a.214.214,0,0,1,.214-.214H4.571v8a1.714,1.714,0,0,0,1.714,1.714H9.714v1.5A.214.214,0,0,1,9.5,16.571Zm4.571-3.429H6.5a.214.214,0,0,1-.214-.214v-11A.214.214,0,0,1,6.5,1.714h3.786V4.857a.857.857,0,0,0,.857.857h3.143v7.214a.214.214,0,0,1-.214.214ZM14.286,4H12V1.714h.344a.214.214,0,0,1,.152.063L14.223,3.5a.214.214,0,0,1,.063.152Z"/>
                        </svg>
                    </div>
                    <input
                        id='purl-input-link'
                        class='purl-input'
                        type="text"
                        readonly
                        value='Loading...'
                    />
                    <label for="purl-input-link">Copy Link</label>
                    <span class='purl-feedback'>Copied!</span>
                </button>
            </div>
        </div>
        `;
    }
}