setTimeout(initFriendBuyPUrl, 1000);
function initFriendBuyPUrl() {
    var siblingContainer = document.querySelector('.sc-bdfBwQ.sc-gsTCUz.bEDtzz.dFfFa-d');
    console.log(siblingContainer);
    if (siblingContainer) {
        appendWidget(siblingContainer);
    }
    function appendWidget(el) {
        // Appending the widget should happen first, before querying elements
        el.insertAdjacentHTML('afterend', createWidget());
        var pUrlInput = document.getElementById('purl-input-link');
        var pUrlButton = document.querySelector('.purl-button');
        var pUrlFeedBackEl = document.querySelector('.purl-feedback');
        var pUrlLabel = document.querySelector('label[for="purl-input-link"]');
        var els = { pUrlInput: pUrlInput, pUrlButton: pUrlButton, pUrlFeedBackEl: pUrlFeedBackEl, pUrlLabel: pUrlLabel };
        pUrlButton.addEventListener('click', onShareClick.bind(null, els));
        // Generate PURL from FB API
        var response = getLink()
            .then(assignReferralValue.bind(null, els))["catch"](console.error);
        response.then(console.log);
    }
    function getLink() {
        var url = 'https://us-central1-mario-luevanos.cloudfunctions.net/api/byte/purl';
        return fetch(url, {
            method: 'POST',
            body: JSON.stringify(getPayload()),
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(function (response) { return response.json(); })
            .then(function (data) { return data.referral_link; });
    }
    function assignReferralValue(_a, referralLink) {
        var pUrlInput = _a.pUrlInput;
        if (referralLink && referralLink.trackable_link) {
            pUrlInput.value = referralLink.trackable_link;
        }
        return referralLink;
    }
    function getPayload() {
        var email_address = getEmail();
        var widget_id = 73808;
        return { widget_id: widget_id, email_address: email_address };
    }
    // Capture the email on the first step
    function getEmail() {
        var email = 'mluevanos@byteme.com';
        if (isValidateEmail(email)) {
            return email;
        }
        return '';
    }
    function isValidateEmail(email) {
        var pattern = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (!email || !pattern.test(email.trim())) {
            return false;
        }
        else {
            return true;
        }
    }
    function onShareClick(els, event) {
        var pUrlInput = els.pUrlInput;
        navigator.clipboard.writeText(pUrlInput.value)
            .then(onCopyClipboard.bind(null, els))["catch"](console.error);
    }
    function onCopyClipboard(els) {
        var pUrlButton = els.pUrlButton, pUrlLabel = els.pUrlLabel, pUrlFeedBackEl = els.pUrlFeedBackEl;
        pUrlLabel.innerText = 'Copied!';
        pUrlFeedBackEl.classList.add('is-copied');
        pUrlButton.classList.add('is-copied');
        pUrlFeedBackEl.addEventListener('animationend', onCopyAnimationEnd.bind(null, els));
    }
    function onCopyAnimationEnd(els, event) {
        console.log(event);
        var pUrlButton = els.pUrlButton, pUrlLabel = els.pUrlLabel, pUrlFeedBackEl = els.pUrlFeedBackEl;
        if (event.animationName === 'copied') {
            setTimeout(function () {
                pUrlButton.classList.remove('is-copied');
                pUrlFeedBackEl.classList.remove('is-copied');
                pUrlLabel.innerText = 'Copy Link';
            }, 0);
            pUrlFeedBackEl.removeEventListener('animationend', onCopyAnimationEnd.bind(null, els));
        }
    }
    function createWidget() {
        return "\n        <div class=\"purl-friendbuy-widget\">\n            <h3>Personal Referral Link</h3>\n            <div class=\"purl-text\">\n                <h4>Get $200 Amazon Gift Card</h4>\n                <p>Give all your friends 75% off their impression kit and $100 off their aligners. In return, <strong>we\u2019ll give you $200</strong> every time one of them orders a plan. It\u2019s only fair. Share away and cash in on those rewards.</p>\n            </div>\n            <div class=\"purl-share\">\n                <h4>Share This Link</h4>\n                <button class='purl-button'>\n                    <div class='purl-icon'>\n                        <svg\n                            xmlns=\"http://www.w3.org/2000/svg\"\n                            width=\"16\"\n                            height=\"18.286\"\n                            viewBox=\"0 0 16 18.286\"\n                        >\n                            <path d=\"M15.5,2.355,13.645.5A1.714,1.714,0,0,0,12.433,0H6.286A1.714,1.714,0,0,0,4.571,1.714V3.429H1.714A1.714,1.714,0,0,0,0,5.143V16.571a1.714,1.714,0,0,0,1.714,1.714h8a1.714,1.714,0,0,0,1.714-1.714V14.857h2.857A1.714,1.714,0,0,0,16,13.143V3.567a1.714,1.714,0,0,0-.5-1.212Zm-6,14.216H1.929a.214.214,0,0,1-.214-.214v-11a.214.214,0,0,1,.214-.214H4.571v8a1.714,1.714,0,0,0,1.714,1.714H9.714v1.5A.214.214,0,0,1,9.5,16.571Zm4.571-3.429H6.5a.214.214,0,0,1-.214-.214v-11A.214.214,0,0,1,6.5,1.714h3.786V4.857a.857.857,0,0,0,.857.857h3.143v7.214a.214.214,0,0,1-.214.214ZM14.286,4H12V1.714h.344a.214.214,0,0,1,.152.063L14.223,3.5a.214.214,0,0,1,.063.152Z\"/>\n                        </svg>\n                    </div>\n                    <input\n                        id='purl-input-link'\n                        class='purl-input'\n                        type=\"text\"\n                        readonly\n                        value='Loading...'\n                    />\n                    <label for=\"purl-input-link\">Copy Link</label>\n                    <span class='purl-feedback'>Copied!</span>\n                </button>\n            </div>\n        </div>\n        ";
    }
}
