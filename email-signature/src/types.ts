export interface IClient {
    name: string;
    device: string;
    icon: string;
}

export interface ISignature {
    name: string;
    title: string;
    tel: string;
    tel2: string;
    message: string;
    messageurl: string;
}