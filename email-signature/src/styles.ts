import defaults from './defaults';

export default function styles() {
    const logoWidth = 64;
    const logoHeight = logoWidth * 0.456;
    const columnLogo = `width: ${logoWidth}px; height: ${logoHeight}px;`;
    const textBold = `font-size: ${defaults.fontSize}px; font-family: 'Arial Black', 'Arial Bold', 'Arial', 'Sans-serif'; font-weight: bold; line-height: normal; color: ${defaults.color.text};`;
    const textRegular = `font-size: ${defaults.fontSize}px; font-family: 'Arial', 'Sans-serif'; line-height: normal; color: ${defaults.color.text};`;
    const link = `${textRegular} text-decoration: none; text-decoration: none !important; color: ${defaults.color.text} !important; display: inline-block;`;
    const iconSocialSize = 16;
    return {
        textOpacity: 1,
        fontSize: defaults.fontSize,
        columnLogo,
        textBold,
        textRegular,
        link,
        logoWidth,
        logoHeight,
        iconSocialSize,
        color: defaults.color
    };
}