import Vue, { CreateElement, VNode } from 'vue';

export default Vue.extend({
    props: {
        steps: {
            type: Array as () => string[],
            default: () => ['']
        }
    },
    render(h:CreateElement): VNode {
        const nodes = this.steps.map((step: string, idx: number) => {
            const carat = idx < this.steps.length - 1 ? h('span', { attrs: { class: 'carat' } }, ['>']) : null;
            return h('strong', [step, carat]);
        });
        return h('span', [...nodes]);
    }
});