import Vue, { CreateElement, VNode } from 'vue';
import styles from '../styles';

const {
    link,
    textBold
} : {
    link: string;
    textBold: string;
} = styles();

export default Vue.extend({
    name: 'SignatureLink',
    props: {
        href: {
            type: String
        },
        fontBold: {
            type: Boolean
        },
        removeAnchors: {
            type: Boolean
        }
    },
    render(h: CreateElement): VNode {
        const style = this.fontBold ? link + textBold : link;
        const Span = h('span', { attrs: { style } }, [ this.$slots.default ]);
        const anchorOpts = { 
            attrs: {
                style,
                href: !this.removeAnchors && this.href
            }
        };
        const Anchor = this.removeAnchors || !this.href ? h('span', anchorOpts, [ Span ]) : h('a', anchorOpts, [ Span ]);
        const Wrap = h('span', { attrs: { style } }, [ Anchor ]);
        return Wrap;
    }
});
