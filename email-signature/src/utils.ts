export function formatPhone(phone: string): string {
    const pa = phone.replace(/\D+/g, '').split('');
    const first3 = pa.slice(0, 3).join('');
    const middle3 = pa.slice(3, 6).join('');
    const last4 = pa.slice(6, 10).join('');
    return `${first3}${middle3.length ? "." : ""}${middle3}${last4.length ? "." : ""}${last4}`;
}

/**
 * @param {Function} func Callback
 * @param {Number} wait Number in milleseconds
 * @param {Boolean} immediate If should be executed instantly
 */

export function debounce(
    func: Function, 
    wait: number, 
    immediate: boolean = false
): Function {
    let timeout: null | number;
    return function() {
        let args: any = arguments;
        let later = () => {
            timeout = null;
            // @ts-ignore
            if (!immediate) func.apply(this as Function, args);
        };
        let callNow = immediate && !timeout;
        // @ts-ignore
        clearTimeout(timeout);
        // @ts-ignore
        timeout = setTimeout(later, wait);
        // @ts-ignore
        if(callNow) func.apply(this as Function, args);
    };
}

export function throttle (callback: Function, limit: number) {
    var waiting = false;                      // Initially, we're not waiting
    return function () {                      // We return a throttled function
        if (!waiting) {                       // If we're not waiting
            //@ts-ignore
            callback.apply(this, arguments);  // Execute users function
            waiting = true;                   // Prevent future invocations
            setTimeout(function () {          // After a period of time
                waiting = false;              // And allow future invocations
            }, limit);
        }
    }
}