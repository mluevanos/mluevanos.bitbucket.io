const storageName = 'reviewtrackers';

async function reviews() {
    hydrateData(document.getElementById('byte-months-ago').value)
    addEvents();
}

function clearStorage() {
    Array
        .from(['-12', '-6', '-3', '-1'])
        .map(value => ({ storageKey: storageName, value }))
        .forEach(clearStorageKeyName);
}

function clearStorageKeyName(row) {
    const name = `${row.storageKey}${row.value}`;
    const item = localStorage.getItem(name);
    if (item !== null) {
        switch (row.value) {
            case '-3':
            case '-1':
                localStorage.removeItem(name);
                break;
        }
    }
    // Delete old storage, should remove later this block of code later
    if (`_${row.storageKey}` === `_${storageName}`) {
        const name = `_${row.storageKey}${row.value}`;
        localStorage.removeItem(name);
    }
}

async function hydrateData(monthsAgo = -1) {
    const widget = document.getElementById('byte-site-dist');
    const target = widget.querySelector('.byte-w-rows');
    const storageKey = `${storageName}${monthsAgo}`;

    if (!localStorage.getItem(storageKey)) {
        setLoadingSkeleton(target);
        const data = await getData(monthsAgo);
        const createdAt = new Date();
        localStorage.setItem(storageKey, JSON.stringify({
            data,
            createdAt
        }));
        appendHTML({ data, target });
        setLastUpdatedText(createdAt);
    } else {
        const payload = JSON.parse(localStorage.getItem(storageKey));
        console.log({ payload });
        if (payload && payload.data) {
            const { data, createdAt } = payload;
            console.log({ data, createdAt });
            appendHTML({
                data,
                target
            });
            setLastUpdatedText(createdAt);
        } else if (payload.length) {
            appendHTML({
                data: payload,
                target
            });
        }
    }
}

function addEvents() {
    const form = document.getElementById('review-site-form');
    const select = form.querySelector('select');
    form.addEventListener('submit', onRefreshData);
    select.addEventListener('change', onMonthChange);
}

async function onRefreshData(event) {
    event.preventDefault();
    const monthsAgo = Number(event.target[0].value);

    const storageKey = `${storageName}${monthsAgo}`;
    window.localStorage.removeItem(storageKey);

    await hydrateData(monthsAgo);
}

async function onMonthChange(event) {
    const monthsAgo = Number(event.target.value);
    await hydrateData(monthsAgo);
}

function appendHTML({data, target}) {
    target.innerHTML = '';
    sortReviewsData(data).map(createHTML).forEach(i => target.appendChild(i));
}

function sortReviewsData(data) {
    // Sort by total reviews
    return Object.values(formatData(data)).sort((p, n) => p.totalReviews > n.totalReviews ? -1 : 1)
    .map((r, i, [first]) => {
        // Take the first node, with most reviews and use that as the base for 100%
        return { 
            distribution: r.totalReviews/first.totalReviews, 
            distributionDiff: r.totalReviews/data.length, 
            ...r
        }
    });
}

function formatData(data) {
    return data.reduce((vendor, item) => {
        const key = item.code;
        if (!vendor[key]) {
            vendor[key] = { 
                name: item.name,
                allRatings: [],
                logo: item.logo,
                get averageRating() {
                    const r = vendor[key].allRatings;
                    return r.reduce((p, c) => p + c)/r.length;
                },
                get totalReviews() {
                    return vendor[key].allRatings.length;
                }
            };
        }
        vendor[key].allRatings.push(item.rating);
        return vendor;
    }, {});
}

function createHTML(item) {
    const logo = `
        <figure class="byte-w-logo">
            <img src="${item.logo}" alt="${item.name}"/>
        </figure>
    `;
    const iconStar = `
        <svg class="byte-w-star" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="gold">
            <path d="M15.144,5.439l-4.317-.628L8.9.9A1.041,1.041,0,0,0,7.1.9L5.173,4.812.856,5.439A1,1,0,0,0,.3,7.145l3.123,3.045-.737,4.3a1,1,0,0,0,1.451,1.054L8,13.513l3.861,2.029a1,1,0,0,0,1.451-1.054l-.737-4.3L15.7,7.145a1,1,0,0,0-.554-1.705Z"></path>
        </svg>`;
    const avgRating = `
        <div class="byte-w-avg-rating">${item.averageRating.toFixed(2)} ${iconStar}</div>
    `;
    const distributionBar = `
        <div class="byte-w-distribution-value ${slugify(item.name)}">
            <span style="width: ${item.distribution * 100}%"></span>
        </div>
    `;
    const distributionText = `
        <div class="byte-w-distribution-text">
            (${(item.distributionDiff*100).toFixed(0)}%)
        </div>
    `;
    const totalReviews = `
        <div class="byte-w-total-reviews">
            ${commaSeparated(item.totalReviews)} ${distributionText}
        </div>
    `;

    // Append HTML
    const root = document.createElement('li');
    root.classList.add('byte-w-row');
    root.innerHTML += `
        ${logo} ${avgRating} ${distributionBar} ${totalReviews}
    `;
    return root;
}

function setLoadingSkeleton(target) {
    const html = `
        <li class="byte-w-row loading"></li>
        <li class="byte-w-row loading"></li>
        <li class="byte-w-row loading"></li>
        <li class="byte-w-row loading"></li>
        <li class="byte-w-row loading"></li>
    `;
    target.innerHTML = html;
}

async function getData(monthsAgo) {
    const response = await fetch(`https://us-central1-mario-luevanos.cloudfunctions.net/api/byte/reviewtrackers?data=reviews&months_ago=${monthsAgo}`, {
        method: 'POST',
        body: JSON.stringify({
            email: 'mluevanos@byteme.com',
            password: 'run4reviewTrackers!'
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    }).catch(console.error);
    
    return await response.json();
}

function slugify(text) {
    return text.replace(/ /gi, '-').toLowerCase();
}

function commaSeparated(text) {
    return text.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

}

function setLastUpdatedText(createdAt) {
    const footer = document.querySelector('.byte-w-footer');
    const timeCreated = footer.querySelector('[data-timestamp="created"]');
    const timeAgo = footer.querySelector('[data-timestamp="ago"]')
    const dateCreated = new Date(createdAt);
    const [ day, month, date ] = dateCreated.toString().split(' ');
    const previous = () => {
        return new Date(
            dateCreated.getFullYear(),
            dateCreated.getMonth(),
            dateCreated.getDate(),
            dateCreated.getHours(),
            dateCreated.getMinutes(),
            dateCreated.getSeconds()
        );
    };
    const current = new Date();

    timeCreated.innerText = `Last Updated—${day} ${month} ${date}`;
    timeAgo.innerText = `(${timeDifference(current, previous())})`;
}

function timeDifference(current, previous) {
    const msPerMinute = 60 * 1000;
    const msPerHour = msPerMinute * 60;
    const msPerDay = msPerHour * 24;
    const msPerMonth = msPerDay * 30;
    const msPerYear = msPerDay * 365;
    const elapsed = current - previous;
    
    if (elapsed < msPerMinute) {
         return `${Math.round(elapsed/1000)} seconds ago`;
    } else if (elapsed < msPerHour) {
         return `${Math.round(elapsed/msPerMinute)} minutes ago`;
    } else if (elapsed < msPerDay ) {
         return `${Math.round(elapsed/msPerHour)} hours ago`;
    } else if (elapsed < msPerMonth) {
         return `approximately ${Math.round(elapsed/msPerDay)} days ago`;
    } else if (elapsed < msPerYear) {
         return `approximately ${Math.round(elapsed/msPerMonth)} months ago`;
    } else {
         return `approximately ${Math.round(elapsed/msPerYear)} years ago`;
    }
}

window.addEventListener('unload', clearStorage);

export default reviews;