export default async function metrics(createContainer) {
    const widget = createContainer('metrics');
    async function getData() {
        const response = await fetch('metrics.json');
        const data = await response.json();
        return data;
    }
    const data = await getData();
    const div = `
        <ul>
            <li><strong>Avg Rating</strong> <span>${data.avg_rating}</span></li>
            <li><strong>Overall Review Count</strong> <span>${data.overall_review_count}</span></li>
            <li><strong>Total Reviews</strong> <span>${data.total_reviews}</span></li>
        </ul>
        <div class="ratings">
            ${
                Object.entries(data.ratings).map(([key, val], idx) => {
                    return `<div class="rating">
                        <p>${key.replace(/star/gmi,' Star')}</p>   
                        <div class="stars star-${idx + 1}">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <p>${val}</p>
                    </div>`;
                }).join(' ')
            }
        </div>
    `;
    const root = document.createElement('div');
    root.classList.add('row');
    root.innerHTML += div;
    widget.appendChild(root)
}
