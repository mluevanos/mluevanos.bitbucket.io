export default async function sources(createContainer) {
    const widget = createContainer('sources');
    async function getData() {
        const response = await fetch('sources.json');
        const data = await response.json();
        const { sources } = data._embedded;
        return sources;
    }
    (await getData())
        .map(item => {
            const img = `<img src="${item.image}" alt="${item.name}"/>`;
            const h5 = `<h5>${item.name}</h5>`;
            const root = document.createElement('div');
            
            root.classList.add('row');
            root.innerHTML += h5 + img;
            return root;
        })
        .forEach(i => widget.appendChild(i));
}